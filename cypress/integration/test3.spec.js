/// <reference types="Cypress" />
describe('Test 3 - Delete the task', function () {
    it('Delete a task from 1 task card', () => {
        cy.visit('http://192.168.99.100:8081');
        cy.get('[href="/task/add"]').click();
        cy.get('#summary').type('Test 1');
        cy.get('#detail').type('Try to create test 1');
        cy.get('#priority').select('Low');
        cy.get('#duedate').type('2020-01-27');
        cy.get('.btn').click();

        cy.get('[class="card border-primary"]').should('have.length', 1);
        cy.get(':nth-child(2) > .card-header > .row > :nth-child(2) > .badge').should('contain.text', 'Priority: Low').and('have.class', 'badge float-right badge-success');
        cy.get(':nth-child(2) > .card-body > .card-title').should('contain.text', 'Test 1');
        cy.get('body > div > div > div.card-body > p').should('contain.text', 'Try to create test 1').and('have.class', 'card-text');
        cy.get('body > div > div > div.card-footer > div > div:nth-child(2) > ul > li:nth-child(2) > a > i').should('have.class', 'fas fa-trash-alt').click();

        cy.get('.container > :nth-child(2)').should('contain.text', 'Are you sure you want to delete this task?');
        cy.get(':nth-child(2) > .badge').should('contain.text', 'Priority: Low').and('have.class', 'badge float-right badge-success');
        cy.get('body > div > div > div.card-body > h5').should('contain.text', 'Test 1').and('have.class', 'card-title');
        cy.get('body > div > div > div.card-body > p').should('contain.text', 'Try to create test 1').and('have.class', 'card-text');
        cy.get('body > div > div > form > input').should('have.value', 'Delete Task').click();
        cy.get('[class="card border-primary"]').should('have.length', 0);
    })

    it('Delete a task from 2 task card', () => {
        cy.visit('http://192.168.99.100:8081');
        cy.get('[href="/task/add"]').click();
        cy.get('#summary').type('Test 1');
        cy.get('#detail').type('Try to create test 1');
        cy.get('#priority').select('Low');
        cy.get('#duedate').type('2020-01-27');
        cy.get('.btn').click();

        cy.get('[href="/task/add"]').click();
        cy.get('#summary').type('Test 2');
        cy.get('#detail').type('Try to create test 2');
        cy.get('#priority').select('Low');
        cy.get('#duedate').type('2020-02-27');
        cy.get('.btn').click();

        cy.get('[class="card border-primary"]').should('have.length', 2);
        cy.get(':nth-child(2) > .card-header > .row > :nth-child(2) > .badge').should('contain.text', 'Priority: Low').and('have.class', 'badge float-right badge-success');
        cy.get(':nth-child(2) > .card-body > .card-title').should('contain.text', 'Test 1');
        cy.get('body > div > div:nth-child(2) > div.card-body > p').should('contain.text', 'Try to create test 1').and('have.class', 'card-text');
        cy.get('body > div > div:nth-child(2) > div.card-footer > div > div:nth-child(2) > ul > li:nth-child(2) > a > i').should('have.class', 'fas fa-trash-alt').click();

        cy.get('.container > :nth-child(2)').should('contain.text', 'Are you sure you want to delete this task?');
        cy.get(':nth-child(2) > .badge').should('contain.text', 'Priority: Low').and('have.class', 'badge float-right badge-success');
        cy.get('body > div > div > div.card-body > h5').should('contain.text', 'Test 1').and('have.class', 'card-title');
        cy.get('body > div > div > div.card-body > p').should('contain.text', 'Try to create test 1').and('have.class', 'card-text');
        cy.get('body > div > div > form > input').should('have.value', 'Delete Task').click();
        cy.get('[class="card border-primary"]').should('have.length', 1);
        cy.get(':nth-child(2) > .card-body > .card-title').should('contain.text', 'Test 2');
    })

    it('Cancel the delete task', () => {
        cy.get('[class="card border-primary"]').should('have.length', 1);
        cy.get(':nth-child(2) > .card-header > .row > :nth-child(2) > .badge').should('contain.text', 'Priority: Low').and('have.class', 'badge float-right badge-success');
        cy.get(':nth-child(2) > .card-body > .card-title').should('contain.text', 'Test 2');
        cy.get('body > div > div:nth-child(2) > div.card-body > p').should('contain.text', 'Try to create test 2').and('have.class', 'card-text');
        cy.get('body > div > div:nth-child(2) > div.card-footer > div > div:nth-child(2) > ul > li:nth-child(2) > a > i').should('have.class', 'fas fa-trash-alt').click();

        cy.get('.container > :nth-child(2)').should('contain.text', 'Are you sure you want to delete this task?');
        cy.get(':nth-child(2) > .badge').should('contain.text', 'Priority: Low').and('have.class', 'badge float-right badge-success');
        cy.get('body > div > div > div.card-body > h5').should('contain.text', 'Test 2').and('have.class', 'card-title');
        cy.get('body > div > div > div.card-body > p').should('contain.text', 'Try to create test 2').and('have.class', 'card-text');
        cy.get('body > div > div > form > a').should('contain.text', 'Cancel').click();
        cy.get('[class="card border-primary"]').should('have.length', 1);
        cy.get(':nth-child(2) > .card-body > .card-title').should('contain.text', 'Test 2');
    })

    //delete tasks before go to next test
    it('Delete all task before go to next test', () => {
        cy.get('body > div > div > div.card-footer > div > div:nth-child(2) > ul > li:nth-child(2) > a > i').should('have.class', 'fas fa-trash-alt').click();
        cy.get('.container > :nth-child(2)').should('contain.text', 'Are you sure you want to delete this task?');
        cy.get(':nth-child(2) > .badge').should('contain.text', 'Priority: Low').and('have.class', 'badge float-right badge-success');
        cy.get('body > div > div > div.card-body > h5').should('contain.text', 'Test 2').and('have.class', 'card-title');
        cy.get('body > div > div > div.card-body > p').should('contain.text', 'Try to create test 2').and('have.class', 'card-text');
        cy.get('body > div > div > form > input').should('have.value', 'Delete Task').click();
        cy.get('[class="card border-primary"]').should('have.length', 0);
    })
})