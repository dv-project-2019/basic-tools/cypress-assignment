/// <reference types="Cypress" />
describe('Test 4 - Mark the task as complete', function () {
    it('Mark the task as complete - 1 task', () => {
        cy.visit('http://192.168.99.100:8081');
        cy.get('[href="/task/add"]').click();
        cy.get('#summary').type('Test 1');
        cy.get('#detail').type('Try to create test 1');
        cy.get('#priority').select('Low');
        cy.get('#duedate').type('2020-01-27');
        cy.get('.btn').click();

        cy.get(':nth-child(2) > .card-header > .row > :nth-child(2) > .badge').should('contain.text', 'Priority: Low').and('have.class', 'badge float-right badge-success');
        cy.get(':nth-child(2) > .card-body > .card-title').should('contain.text', 'Test 1');
        cy.get('body > div > div > div.card-body > p').should('contain.text', 'Try to create test 1').and('have.class', 'card-text');
        cy.get(':nth-child(2) > .card-footer > .row > :nth-child(1) > h4 > .badge').should('contain.text', 'Incomplete');
        cy.get('body > div > div > div.card-footer > div > div:nth-child(2) > ul > li:nth-child(3) > form > button > i').should('have.class', 'fas fa-check').click();
        cy.get(':nth-child(2) > .card-footer > .row > :nth-child(1) > h4 > .badge').should('contain.text', 'Complete');
        cy.get('body > div > div > div.card-footer > div > div:nth-child(2)').should('not.have.class', 'fas fa-edit');
        cy.get('body > div > div > div.card-footer > div > div:nth-child(2) > ul > li:nth-child(2) > form > button > i').should('have.class', 'fas fa-redo-alt');
    })

    it('Mark the task as complete - 2 task', () => {
        cy.visit('http://192.168.99.100:8081');
        cy.get('[href="/task/add"]').click();
        cy.get('#summary').type('Test 2');
        cy.get('#detail').type('Try to create test 2');
        cy.get('#priority').select('Low');
        cy.get('#duedate').type('2020-02-27');
        cy.get('.btn').click();

        cy.get(':nth-child(4) > .card-header > .row > :nth-child(2) > .badge').should('contain.text', 'Priority: Low').and('have.class', 'badge float-right badge-success');
        cy.get('body > div > div:nth-child(4) > div.card-body > h5').should('contain.text', 'Test 2');
        cy.get(':nth-child(4) > .card-footer > .row > :nth-child(1) > h4 > .badge').should('contain.text', 'Incomplete');
        cy.get('body > div > div:nth-child(4) > div.card-footer > div > div:nth-child(2) > ul > li:nth-child(3) > form > button > i').should('have.class', 'fas fa-check').click();
        cy.get(':nth-child(4) > .card-footer > .row > :nth-child(1) > h4 > .badge').should('contain.text', 'Complete');
        cy.get(':nth-child(4) > .card-footer > .row > :nth-child(2) > .nav').should('not.have.class', 'fas fa-edit');
        cy.get('body > div > div:nth-child(4) > div.card-footer > div > div:nth-child(2) > ul > li:nth-child(1) > a > i').should('have.class', 'fas fa-trash-alt');
        cy.get('body > div > div:nth-child(4) > div.card-footer > div > div:nth-child(2) > ul > li:nth-child(2) > form > button > i').should('have.class', 'fas fa-redo-alt');
    })

    it('Mark the task as Incomplete', () => {
        cy.get(':nth-child(4) > .card-header > .row > :nth-child(2) > .badge').should('contain.text', 'Priority: Low').and('have.class', 'badge float-right badge-success');
        cy.get('body > div > div:nth-child(4) > div.card-body > h5').should('contain.text', 'Test 2');
        cy.get(':nth-child(4) > .card-footer > .row > :nth-child(1) > h4 > .badge').should('contain.text', 'Complete');
        cy.get('body > div > div:nth-child(4) > div.card-footer > div > div:nth-child(2) > ul > li:nth-child(2) > form > button > i').should('have.class', 'fas fa-redo-alt').click();
        cy.get(':nth-child(4) > .card-footer > .row > :nth-child(1) > h4 > .badge').should('contain.text', 'Incomplete');
        cy.get('body > div > div:nth-child(4) > div.card-footer > div > div:nth-child(2) > ul > li:nth-child(1) > a > i').should('have.class', 'fas fa-edit');
        cy.get('body > div > div:nth-child(4) > div.card-footer > div > div:nth-child(2) > ul > li:nth-child(2) > a > i').should('have.class', 'fas fa-trash-alt');
        cy.get('body > div > div:nth-child(4) > div.card-footer > div > div:nth-child(2) > ul > li:nth-child(3) > form > button > i').should('have.class', 'fas fa-check');
        cy.get('body > div > div:nth-child(4) > div.card-footer > div > div:nth-child(2) > ul').should('not.have.class', 'fas fa-redo-alt');
    })

    //delete tasks before go to next test
    it('Delete all task before go to next test', () => {
         cy.get('body > div > div:nth-child(2) > div.card-footer > div > div:nth-child(2) > ul > li:nth-child(1) > a > i').should('have.class', 'fas fa-trash-alt').click();
         cy.get('.container > :nth-child(2)').should('contain.text', 'Are you sure you want to delete this task?');
         cy.get('body > div > div > div.card-body > h5').should('contain.text', 'Test 1').and('have.class', 'card-title');
         cy.get('body > div > div > div.card-body > p').should('contain.text', 'Try to create test 1').and('have.class', 'card-text');
         cy.get('body > div > div > div.card-footer > div > div:nth-child(1) > h4 > span').should('contain.text', 'Complete');
         cy.get('body > div > div > form > input').should('have.value', 'Delete Task').click();
         cy.get('[class="card border-primary"]').should('have.length', 1);
 
         cy.get('body > div > div > div.card-footer > div > div:nth-child(2) > ul > li:nth-child(2) > a > i').should('have.class', 'fas fa-trash-alt').click();
         cy.get('.container > :nth-child(2)').should('contain.text', 'Are you sure you want to delete this task?');
         cy.get(':nth-child(2) > .badge').should('contain.text', 'Priority: Low').and('have.class', 'badge float-right badge-success');
         cy.get('body > div > div > div.card-body > h5').should('contain.text', 'Test 2').and('have.class', 'card-title');
         cy.get('body > div > div > div.card-body > p').should('contain.text', 'Try to create test 2').and('have.class', 'card-text');
         cy.get('body > div > div > div.card-footer > div > div:nth-child(1) > h4 > span').should('contain.text', 'Incomplete');
         cy.get('body > div > div > form > input').should('have.value', 'Delete Task').click();
         cy.get('[class="card border-primary"]').should('have.length', 0);
    })
})