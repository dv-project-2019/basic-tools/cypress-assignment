/// <reference types="Cypress" />
describe('Test 2 - Edit the task', function () {
    it('Edit task from 1 task card', () => {
        cy.visit('http://192.168.99.100:8081');
        cy.get('[href="/task/add"]').click();
        cy.get('#summary').type('Test 1');
        cy.get('#detail').type('Try to create test 1');
        cy.get('#priority').select('Normal');
        cy.get('#duedate').type('2020-01-27');
        cy.get('.btn').click();

        cy.get('[class="card border-primary"]').should('have.length', 1);
        cy.get(':nth-child(2) > .card-body > .card-title').should('contain.text', 'Test 1');
        cy.get('body > div > div > div.card-footer > div > div:nth-child(2) > ul > li:nth-child(1) > a > i').click();
        cy.get('#summary').clear();
        cy.get('#summary').type('Test edit');
        cy.get('#detail').clear();
        cy.get('#detail').type('Try to edit');
        cy.get('.btn').click();

        cy.get(':nth-child(2) > .card-header > .row > :nth-child(1)').should('contain.text', 'Due By:')
            .and('contain.text', '2020-01-27');
            cy.get(':nth-child(2) > .card-header > .row > :nth-child(2) > .badge').should('contain.text', 'Priority: Normal').and('have.class', 'badge float-right badge-warning');
        cy.get(':nth-child(2) > .card-body > .card-title').should('contain.text', 'Test edit');
        cy.get(':nth-child(2) > .card-body > .card-text').should('contain.text', 'Try to edit');
        cy.get(':nth-child(2) > .card-footer > .row > :nth-child(1) > h4 > .badge').should('contain.text', 'Incomplete');
    })

    it('Edit 1 task from 2 task card', () => {
        cy.visit('http://192.168.99.100:8081');
        cy.get('[href="/task/add"]').click();
        cy.get('#summary').type('Test 2');
        cy.get('#detail').type('Try to create test 2');
        cy.get('#priority').select('Low');
        cy.get('#duedate').type('2020-02-27');
        cy.get('.btn').click();

        cy.get('body > div > div:nth-child(4) > div.card-body > h5').should('contain.text', 'Test 2');
        cy.get('body > div > div:nth-child(4) > div.card-footer > div > div:nth-child(2) > ul > li:nth-child(1) > a > i').click();
        cy.get('#summary').clear();
        cy.get('#summary').type('Test edit2');
        cy.get('#detail').clear();
        cy.get('#detail').type('Try to edit2');
        cy.get('.btn').click();

        cy.get('[class="card border-primary"]').should('have.length', 2);
        cy.get(':nth-child(4) > .card-header > .row > :nth-child(1)').should('contain.text', 'Due By:')
            .and('contain.text', '2020-02-27');
        cy.get('body > div > div:nth-child(4) > div.card-header > div > div:nth-child(2) > span').should('contain.text', 'Priority: Low').and('have.class', 'badge float-right badge-success');
        cy.get('body > div > div:nth-child(4) > div.card-body > h5').should('contain.text', 'Test edit2');
        cy.get('body > div > div:nth-child(4) > div.card-body > p').should('contain.text', 'Try to edit2').and('have.class', 'card-text');
        cy.get(':nth-child(4) > .card-footer > .row > :nth-child(1) > h4 > .badge').should('contain.text', 'Incomplete');
    })

    //delete tasks before go to next test
    it('Delete all task before go to next test', () => {
        cy.get('body > div > div:nth-child(2) > div.card-footer > div > div:nth-child(2) > ul > li:nth-child(2) > a > i').should('have.class', 'fas fa-trash-alt').click();
        cy.get('.container > :nth-child(2)').should('contain.text', 'Are you sure you want to delete this task?');
        cy.get('body > div > div > div.card-body > h5').should('contain.text', 'Test edit').and('have.class', 'card-title');
        cy.get('body > div > div > div.card-body > p').should('contain.text', 'Try to edit').and('have.class', 'card-text');
        cy.get('body > div > div > form > input').should('have.value', 'Delete Task').click();
        cy.get('[class="card border-primary"]').should('have.length', 1);

        cy.get('body > div > div > div.card-footer > div > div:nth-child(2) > ul > li:nth-child(2) > a > i').should('have.class', 'fas fa-trash-alt').click();
        cy.get('.container > :nth-child(2)').should('contain.text', 'Are you sure you want to delete this task?');
        cy.get(':nth-child(2) > .badge').should('contain.text', 'Priority: Low').and('have.class', 'badge float-right badge-success');
        cy.get('body > div > div > div.card-body > h5').should('contain.text', 'Test edit2').and('have.class', 'card-title');
        cy.get('body > div > div > div.card-body > p').should('contain.text', 'Try to edit2').and('have.class', 'card-text');
        cy.get('body > div > div > form > input').should('have.value', 'Delete Task').click();
        cy.get('[class="card border-primary"]').should('have.length', 0);
    })
})